﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Task_05 : MonoBehaviour
    {
        public Task_04.PlayerState playerState;
        private float movementSpeed;

        private void Update()
        {
            switch(playerState)
            {
                case Task_04.PlayerState.NONE:
                    movementSpeed = 0;
                    Debug.Log(movementSpeed);
                    Debug.Log(playerState);
                    break;

                case Task_04.PlayerState.IDLE:
                    movementSpeed = 1.8658765f;
                    Debug.Log(movementSpeed);
                    Debug.Log(playerState);
                    break;

                case Task_04.PlayerState.WALKING:
                    movementSpeed = 11.8635436f;
                    Debug.Log(movementSpeed);
                    Debug.Log(playerState);
                    break;

                case Task_04.PlayerState.RUNNING:
                    movementSpeed = 22.376373283f;
                    Debug.Log(movementSpeed);
                    Debug.Log(playerState);
                    break;
            }
        }

    }
}