﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{

    public class Task_01 : MonoBehaviour
    {
        bool isPlayerAlive = true;

        public int health = 100;

        public void Update()
        {
            if (isPlayerAlive == true)
            {
                Debug.Log("Du bist am Leben, kämpfe weiter!");
            }
            else if (health >= 10)
            {
                Debug.Log("Deine Kräfte schwinden, regeneriere Leben!");
            }
        }
    }
}