﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Task_02 : MonoBehaviour
    {
        public bool isGrounded = false;

        public bool canJump = true;

        public void Update()
        {
            if(isGrounded == false)
            {
                canJump = false;

                Debug.Log("Du kannst nicht springen, weil du nicht am Boden bist!");
                //Wieso sollte ich nicht springen können, weil ich nicht "grounded" bin? Ergibt das einen Sinn?
            }
        }
    }
}